import { createAsyncThunk, createSlice, PayloadAction } from '@reduxjs/toolkit'

import type { AppState, AppThunk } from '../../app/store'
// import { fetchCount } from './counter-API'

export interface PlayState {
  value: boolean
}

const initialState: PlayState = {
  value: true,
}



export const playerSlice = createSlice({
  name: 'play',
  initialState,
  // The `reducers` field lets us define reducers and generate associated actions
  reducers: {
    play: (state) => {
      state.value = true
    },
    pause: (state) => {
      state.value = false
    },
    
  },
  
})

export const { play, pause } = playerSlice.actions

// The function below is called a selector and allows us to select a value from
// the state. Selectors can also be defined inline where they're used instead of
// in the slice file. For example: `useSelector((state: RootState) => state.counter.value)`
export const selectPlay = (state: AppState) => state.player.value

// We can also write thunks by hand, which may contain both sync and async logic.
// Here's an example of conditionally dispatching actions based on current state.


export default playerSlice.reducer