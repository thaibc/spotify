import "../styles/globals.css";
import { Provider } from "react-redux";
import type { AppProps } from "next/app";
import { SessionProvider } from "next-auth/react";
import { RecoilRoot } from "recoil";
import store from "../app/store";

export default function MyApp({
  Component,
  pageProps: { session, ...pageProps },
}: AppProps) {
  return (
    <SessionProvider session={session}>
      <RecoilRoot>
        <Component {...pageProps} />
      </RecoilRoot>
      {/* <Provider store={store}></Provider> */}
    </SessionProvider>
  );
}
