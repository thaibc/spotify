import type { NextPage } from "next";
import Head from "next/head";
import Image from "next/image";
import Dashboard from "../components/dashboard";
import Counter from "../components/counter";
import { useRouter } from "next/router";
import { useSession, UseSessionOptions } from "next-auth/react";
import { Loader } from "../components/loader";

const Home = () => {
  const router = useRouter();
  const { status, data: session } = useSession({
    required: true,
    onUnauthenticated() {
      router.push("/auth/signin");
    },
  } as any);

  if (status === "loading") {
    return <Loader />;
  }

  return (
    <div>
      <Head>
        <title>Spotify - Dashboard</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <Dashboard />
    </div>
  );
};

export default Home;
