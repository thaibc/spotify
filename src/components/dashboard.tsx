import Body, { Track } from "./body";
import Right from "./right";
import Sidebar from "./sidebar";
import SpotifyWebApi from "spotify-web-api-node";
import { useAppSelector } from "../app/hooks";
import { selectPlay } from "../features/play/play-slice";
import { useRecoilState } from "recoil";
import { playingTrackState } from "../atoms/player-atoms";

const spotifyApi: SpotifyWebApi = new SpotifyWebApi({
  clientId: process.env.SPOTIFY_CLIENT_ID,
});

export default function Dashboard() {
  const [playingTrack, setPlayingTrack] =
    useRecoilState<any>(playingTrackState);
  const chooseTrack = (track: Track) => {
    setPlayingTrack(track);
  };

  return (
    <main className="flex min-h-screen min-w-max lg:pb-24">
      <Sidebar />
      <Body spotifyApi={spotifyApi} chooseTrack={chooseTrack} />
      <Right spotifyApi={spotifyApi} chooseTrack={chooseTrack} />
    </main>
  );
}
