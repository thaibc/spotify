import { useSession } from "next-auth/react";
import { useEffect, useState } from "react";
import SpotifyWebApi from "spotify-web-api-node";
import Counter from "./counter";
import Poster from "./poster";
import Search from "./search";
import Tracks from "./tracks";

interface Props {
  spotifyApi: SpotifyWebApi;
  chooseTrack: (track: Track) => void;
}

interface artists {
  name: string;
}

interface Image {
  url: string;
}

interface Album extends Image {
  artists: artists[];
  images: Image[];
  id: string;
  name: string;
  uri: string;
  popularity: number;
}
export interface Track {
  id: string;
  artists: artists[];
  name: string;
  uri: string;
  album: Album;
  popularity: number;
  albumUrl: string;
  title: string;
  artist: string;
}

export default function Body({ spotifyApi, chooseTrack }: Props) {
  const { data: session } = useSession();
  const accessToken: any = session?.accessToken;
  const [search, setSearch] = useState("");
  const [searchResults, setSearchResults] = useState([]);
  const [newReleases, setNewReleases] = useState([]);

  useEffect(() => {
    if (!accessToken) return;
    spotifyApi.setAccessToken(accessToken);
  }, [accessToken]);

  //searching...
  useEffect(() => {
    if (!search) return setSearchResults([]);
    if (!accessToken) return;

    spotifyApi.searchTracks(search).then((res: any) => {
      setSearchResults(
        res.body.tracks.items.map((track: Track) => {
          console.log(track);

          return {
            id: track.id,
            artist: track.artists[0].name,
            title: track.name,
            uri: track.uri,
            albumUrl: track.album.images[0].url,
            popularity: track.popularity,
          };
        })
      );
    });
  }, [search, accessToken]);

  //New Releases...
  useEffect(() => {
    if (!accessToken) return;

    spotifyApi.getNewReleases().then((res: any) => {
      setNewReleases(
        res.body.albums.items.map((album: Album) => {
          return {
            id: album.id,
            artist: album.artists[0].name,
            title: album.name,
            uri: album.uri,
            albumUrl: album.images[0].url,
          };
        })
      );
    });
  }, [accessToken]);

  return (
    <section className=" bg-black ml-20 py-4 space-y-2 flex-grow md:max-w-5xl">
      <Search search={search} setSearch={setSearch} />
      <div
        className="grid overflow-y-scroll scrollbar-hide h-96 py-4 grid-cols-2 
      lg:grid-cols-3 xl:grid-cols-4 gap-x-4 gap-y-8 p-4 "
      >
        {searchResults.length === 0
          ? newReleases
              .slice(0, 4)
              .map((track: Track) => (
                <Poster
                  key={track.id}
                  track={track}
                  chooseTrack={chooseTrack}
                />
              ))
          : searchResults
              .slice(0.4)
              .map((track: Track) => (
                <Poster
                  key={track.id}
                  track={track}
                  chooseTrack={chooseTrack}
                />
              ))}
      </div>
      <div className="flex gap-x-8 absolute min-w-full md:relative ml-6">
        {/*Genres*/}
        <div className="hidden xl:inline max-w-[234px]">
          <h2 className="text-white font-bold mb-3">Genres</h2>
          <div className="flex gap-x-2 gap-y-2.5 flex-wrap mb-3">
            <div className="genre">Classic</div>
            <div className="genre">House</div>
            <div className="genre">Minimal</div>
            <div className="genre">Hip-hop</div>
            <div className="genre">Electronic</div>
            <div className="genre">Chillout</div>
            <div className="genre">Blues</div>
            <div className="genre">Country</div>
            <div className="genre">Techno</div>
          </div>
          <button className="btn hover:bg-opacity-100 bg-opacity-80">
            All Genres
          </button>
        </div>

        {/*Tracks */}
        <div className="w-full pr-11">
          <h2 className="text-white font-bold mb-3 ">
            {searchResults.length === 0 ? "New Releases" : "Tracks"}
          </h2>
          <div className="space-y-3 border-2 border-[#262626] rounded-2xl p-3 bg-[#0D0D0D] overflow-y-scroll h-[1000px] md:h-96 scrollbar-thin scrollbar-thumb-gray-600 scrollbar-thumb-rounded hover:scrollbar-thumb-gray-500 w-[732px]">
            {searchResults.length === 0
              ? newReleases
                  .slice(4, newReleases.length)
                  .map((track: Track) => (
                    <Tracks
                      key={track.id}
                      track={track}
                      chooseTrack={chooseTrack}
                    />
                  ))
              : searchResults
                  .slice(4, searchResults.length)
                  .map((track: Track) => (
                    <Tracks
                      key={track.id}
                      track={track}
                      chooseTrack={chooseTrack}
                    />
                  ))}
          </div>
        </div>
      </div>
    </section>
  );
}
