import SpotifyWebApi from "spotify-web-api-node";
import { Track } from "./body";
import { HiOutlineShieldCheck } from "react-icons/hi";
import { MdOutlineSettings } from "react-icons/md";
import { BiBell } from "react-icons/bi";
import { ViewGridIcon } from "@heroicons/react/solid";
import Dropdown from "./dropdown";
import RecentlyPlayed from "./recently-player";
import { useSession } from "next-auth/react";
import { useEffect, useState } from "react";

export interface RightProps {
  spotifyApi: SpotifyWebApi;
  chooseTrack: (track: Track) => void;
}

export default function Right({ spotifyApi, chooseTrack }: RightProps) {
  const { data: session } = useSession();
  const accessToken: any = session?.accessToken;
  const [recentlyPlayed, setRecentlyPlayed] = useState([]);

  console.log(recentlyPlayed);

  // Recently Played Tracks...
  useEffect(() => {
    if (!accessToken) return;

    spotifyApi.getMyRecentlyPlayedTracks().then((res: any) => {
      console.log(res);

      setRecentlyPlayed(
        res.body.items.map(({ track }: any) => {
          return {
            id: track.id,
            artist: track.artists[0].name,
            title: track.name,
            uri: track.uri,
            albumUrl: track.album.images[0].url,
          };
        })
      );
    });
  }, [accessToken]);

  return (
    <section className="p-4 space-y-8 pr-8">
      <div className="flex space-x-2 items-center justify-between">
        {/*Icons */}
        <div className="flex items-center space-x-4 border-2 border-[#262626] rounded-full h-12 py-3 px-4 ">
          <HiOutlineShieldCheck className="text-[#ccc] text-xl " />
          <MdOutlineSettings className="text-[#ccc] text-xl " />
          <BiBell className="text-[#ccc] text-xl " />
        </div>
        {/* Profile */}
        <Dropdown />
      </div>
      {/* Recently Played Tracks */}
      <div className="bg-[#0D0D0D] border-2 border-[#262626] p-4 rounded-xl space-y-4">
        <div className="flex items-center justify-between">
          <h4 className="text-white font-semibold text-sm">Recently Played</h4>
          <ViewGridIcon className="text-[#686868] h-6" />
        </div>

        <div className="space-y-4 overflow-y-scroll overflow-x-hidden h-[250px] md:h-[400px] scrollbar-hide">
          {recentlyPlayed.map((track: Track, index: any) => (
            <RecentlyPlayed
              key={index}
              track={track}
              chooseTrack={chooseTrack}
            />
          ))}
        </div>
        <button className="text-[#CECECE] bg-[#1A1A1A] text-[13px] py-3.5 px-4 rounded-2xl w-full font-bold bg-opacity-80 hover:bg-opacity-100 transition ease-out">
          View All
        </button>
      </div>
    </section>
  );
}
